// ==UserScript==
// @author         Perringaiden
// @name           IITC plugin: Highlight field anchors that need recharging
// @category       Highlighter
// @version        0.4
// @description    Use the portal fill color to denote if the portal needs recharging and how much. Yellow: above 85%. Orange: above 50%. Red: above 15%. Magenta: below 15%.
// @id             highlight-wolf-recharge
// @updateURL      https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-recharge.meta.js
// @downloadURL    https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-recharge.user.js
// @match          https://intel.ingress.com/*
// @grant          none
// ==/UserScript==