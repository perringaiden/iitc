// ==UserScript==
// @author         Perringaiden
// @name           IITC plugin: Machina Tools
// @category       Misc
// @version        0.7
// @description    Machina investigation tools
// @id             misc-wolf-machina
// @updateURL      https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-machina.meta.js
// @downloadURL    https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-machina.user.js
// @match          https://intel.ingress.com/*
// @grant          none
// ==/UserScript==