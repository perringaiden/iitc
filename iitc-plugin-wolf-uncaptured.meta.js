// ==UserScript==
// @id             iitc-plugin-wolf-uncaptured@Perringaiden
// @name           IITC plugin: Non Captured/Visited/Scout Highlighters
// @category       Misc
// @version        0.7
// @downloadURL    https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-uncaptured.user.js
// @updateURL      https://bitbucket.org/perringaiden/iitc/raw/master/iitc-plugin-wolf-uncaptured.meta.js
// @include        *://*.ingress.com/*
// @match          *://*.ingress.com/*
// @grant          none
// ==/UserScript==